"use strict";
exports.__esModule = true;
var engine_1 = require("@runtime/engine");
var Yote = /** @class */ (function () {
    function Yote() {
    }
    Yote.prototype.title = function () {
        return "kala ma";
    };
    return Yote;
}());
engine_1.Engine.setGlobalProperty("Hello", new Yote);
engine_1.Engine.load("qrc:/main.qml");
engine_1.Engine.run();
console.log(engine_1.Engine.packagePath);
