export declare namespace Engine {
    function setGlobalProperty(name: string, val: any): void;
    function run(): bigint;
    function load(path: string): void;
    const packagePath: string;
}
