// SPDX-FileCopyrightText: 2021 Jan Blackquill <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QObject>
#include <QJSValue>
#include <QJSEngine>

class Caller : public QObject
{
    Q_OBJECT

    std::function<QJSValue(QJSValue)> f;

public:
    Caller(std::function<QJSValue(QJSValue)> f) : QObject(nullptr), f(f)
    {

    }

    Q_INVOKABLE QJSValue call(QJSValue v) {
        return f(v);
    }
};

QJSValue newFunction(QJSEngine* forEngine, std::function<QJSValue(QJSValue)> f);