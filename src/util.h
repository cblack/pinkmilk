#include <QtCore>
#include <QString>
#include <QChar>
#include <QPair>
#include <QScopeGuard>

typedef QString string;
typedef QChar rune;
#define let const auto
#define var auto
#define lambda [=]()
#define str(x) QStringLiteral(x)
#define _concat_(a, b) a ## b
#define _concat(a, b) _concat_(a, b)
#define defer(thing) var _concat(defer, __LINE__) = qScopeGuard([=]() { thing; });
