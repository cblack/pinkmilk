#include <QGuiApplication>
#include <QJSEngine>
#include <QQmlApplicationEngine>
#include <QDebug>
#include "util.h"
#include "main.h"

let script = R"RJIENRLWEY(
"use strict";
exports.__esModule = true;
require("./engine");
Engine.setGlobalProperty("yeet", "Oh No!");
Engine.load("qrc:/main.qml");
Engine.run();
)RJIENRLWEY";

int main(int argc, char *args[]) {
    QScopedPointer<QGuiApplication> app(new QGuiApplication(argc, args));
    QScopedPointer<PureJSEngine> engine(new PureJSEngine);

    QFile f(app->arguments()[1]);
    Q_ASSERT(f.open(QIODevice::ReadOnly));

    let result = engine->evaluate(f.readAll(), app->arguments()[1]);
    if (result.isError()) {
        qDebug() << result.property("lineNumber").toInt() << ":" << result.toString();
    }
    if (result.isNumber()) {
        return result.toInt();
    }
    return 0;
}