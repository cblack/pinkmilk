// SPDX-FileCopyrightText: 2021 Jan Blackquill <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include "func.h"

QJSValue newFunction(QJSEngine* forEngine, std::function<QJSValue(QJSValue)> f)
{
    auto transformer = forEngine->evaluate(R"(
(function(instance) {
    return function(...args) {
        return instance.call(Array.from(args))
    }
})
)");
    auto transformed = transformer.call({forEngine->newQObject(new Caller(f))});
    return transformed;
}