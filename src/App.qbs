QtApplication {
	files: [
		"main.cpp",
		"main.h",
		"func.cpp",
		"func.h",
		"yeet.qrc",
	]

	Depends { name: "Qt"; submodules: ["qml", "gui"] }
}
