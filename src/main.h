#pragma once

#include <QCoreApplication>
#include <QDebug>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "func.h"

class Yeet : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE Yeet() {}
    Q_INVOKABLE void printHi() { qDebug() << "hi"; }
};

class JSAPIEngine : public QObject
{
    Q_OBJECT

    QQmlApplicationEngine* _parentEngine;

public:
    JSAPIEngine(QQmlApplicationEngine* eng) : QObject(eng), _parentEngine(eng)
    {

    }

    Q_INVOKABLE void load(const QString& name) {
        _parentEngine->load(name);
    }
    Q_INVOKABLE void setGlobalProperty(QString name, QJSValue val) {
        _parentEngine->globalObject().setProperty(name, val);
    }
    Q_INVOKABLE int run() {
        return QCoreApplication::exec();
    }
};

class PureJSEngine : public QQmlApplicationEngine
{
    Q_OBJECT

    Q_PROPERTY(JSAPIEngine* Engine READ jsapiEngine CONSTANT)
    QScopedPointer<JSAPIEngine> eng;

public:
    JSAPIEngine* jsapiEngine() const
    {
        return eng.data();
    }
    Q_INVOKABLE PureJSEngine(QObject *parent = nullptr) : QQmlApplicationEngine(parent), eng(new JSAPIEngine(this)) {
        this->installExtensions(
            QJSEngine::AllExtensions
        );

        const auto collection = QMap<QString,QJSValue> {
            {"@runtime/engine", this->newQObject(this)}
        };

        const auto yote = this->newQObject(this);
        this->setObjectOwnership(this, QQmlEngine::CppOwnership);
        this->globalObject().setProperty("require", newFunction(this, [collection](QJSValue f) -> QJSValue {
            qDebug() << "import" << f.property(0).toString();
            auto str = f.property(0).toString();
            if (collection.contains(str)) {
                return collection[str];
            }
            qFatal("bad import");
        }));
        this->globalObject().setProperty("exports", this->newObject());

    }
};