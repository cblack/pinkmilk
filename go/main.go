package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
)

type App struct {
	Options map[string]string `json:"options"`
}

func main() {
	app := App{}
	data, err := ioutil.ReadFile("app.json")
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(data, &app)
	if err != nil {
		log.Fatal(err)
	}

	cmd := exec.Command("tsc", "--module", "commonjs")
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}
